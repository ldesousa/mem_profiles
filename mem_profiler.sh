#!/bin/bash
# 
# Logs the memory used by a programme and computes basic statistics with 
# GeoPandas.
#
# Copyright (c) 2020 Luís Moreira de Sousa. All rights reserved.
# Any use of this software constitutes full acceptance of all terms of the
# document licence.
# SPDX-License-Identifier: EUPL-1.2

# Just in case
rm mem.txt

# Replace run-my-programme with the right command, leave the echo $! in place
run-my-programme & echo $!
echo "Monitoring process "$!

while [ -f /proc/$!/stat ]; 
do
    cat /proc/$!/stat | cut -d')' -f2 | cut -d' ' -f22 >> mem.txt
    sleep 2s
done

python3 stats.py mem.txt
