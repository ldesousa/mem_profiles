#!/usr/bin/python3
#
# Computes basic statistics from a memory usage log file.
#
# Copyright (c) 2020 Luís Moreira de Sousa. All rights reserved.
# Any use of this software constitutes full acceptance of all terms of the
# document licence.
# SPDX-License-Identifier: EUPL-1.2

import pandas
import sys
from decimal import Decimal

data = pandas.read_csv(sys.argv[1], header=None)

print("Average: ", "{:.2E}".format(Decimal(data[0].mean())))
print("Maximum: ", "{:.2E}".format(Decimal(float(data[0].max()))))
print("StdDev: ",  "{:.2E}".format(Decimal(data[0].std())))




