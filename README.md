Memory Profiler
===============

Simple programme to profile the memory used by a programme. Presents basic statistics computed with Pandas.

Usage
-----

Edit the file `mem_profile.sh` adding in the command to run the programme to monitor.

Licence
-------
This suite of programmes is released under the [EUPL 1.2 licence](https://joinup.ec.europa.eu/community/eupl/og_page/introduction-eupl-licence). For full details please consult the LICENCE file. 

